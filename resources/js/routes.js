import Users from './components/Users';
import Profile from './components/Profile';
import Dashboard from './components/Dashboard';
import Developer from './components/Developer';
import NotFound from './components/NotFound';
import Role from './components/admin/Role';
import assignRole from './components/admin/assignRole';
import Home from './components/Home';

export default {
    mode: 'history',
    linkActiveClass: 'font-semibold',
    routes: [
        
        {
            path: '/users',
            component: Users,
            name:'users'
           
        }, 
         {
            path: '/profile',
            component: Profile,
            name:'profile'
           
        },
        {
        path: '/home',
        component: Home,
        name: 'home'

    },
        {
            path: '/dashboard',
            component: Dashboard,
           name:'dashboard'
        },
       
         {
            path: '/developer',
            component: Developer,
            name:'developer'
           
        },
         {
            path: '/role',
            component: Role,
            name:'role'
           
        },
        {
            path: '*',
            component:NotFound,
           name:'notfound'
        } ,
         {
            path: '/assignrole',
            component:assignRole,
           name:'assignRole'
        } 
    ]
}
